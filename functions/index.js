const functions = require('firebase-functions');
//express
const express = require('express');
const { db } = require('./utils/admin');
const app = express();
//Import function
//User handle
const { SignUp, Login } = require('./handlers/user')
//Todo handle
const { addTodo, editTodo, completeTodo, getAllTodos, deleteTodo} = require('./handlers/todo');

//Import FBAuth
const FBAuth = require('./utils/fbAuth');
//Setting API
//User routes
app.post('/signup',SignUp);
app.post('/login',Login);

//Todo route
app.get('/todos', FBAuth, getAllTodos);
app.post('/add', FBAuth, addTodo);
app.post('/edit/:todoID', FBAuth, editTodo);
app.post('/changeStatus/:todoID', FBAuth, completeTodo);
app.post('/delete/:todoID',FBAuth, deleteTodo)

exports.api = functions.region("europe-west1").https.onRequest(app);
