//check empty data
exports.isEmpty = ( data ) => {
    if( data.trim() === "" ) {
        return true;
    }
    else {
        return false;
    }
}

//check validate data

exports.isEmail = (email) => {
    // eslint-disable-next-line no-useless-escape
    const regEx = "^[a-z][a-z0-9_\.]{5,32}@[a-z0-9]{2,}(\.[a-z0-9]{2,4}){1,2}$";
    if( email.match(regEx) ) return true;
    return false;
}
