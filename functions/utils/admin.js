const admin = require('firebase-admin');
const firebase = require('firebase');
//initial App
admin.initializeApp();
const config = require('./config');
firebase.initializeApp(config);
//database
const db = firebase.firestore();

module.exports = { admin, db }
