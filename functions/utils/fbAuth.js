const { admin } = require("./admin")
const { db } = require ("./admin")

module.exports = (req, res, next) => {
    let IDToken;

    if( req.headers.authorization && req.headers.authorization.startsWith('HV ') ) {
        IDToken = req.headers.authorization.split("HV ")[1];
    }
    else {
        return res.status(403).json({
            error:"Unauthorized"
        })
    }
    admin.auth().verifyIdToken(IDToken)
        .then(decodeToken => {
            req.user = decodeToken;
            console.log(req.user.uid);
            return db.collection("users").where("email","==",req.user.email)
                .limit(1)
                .get()
        })
        .then(rs => {
            req.user.email = rs.docs[0].data().email;
            return next();
        })
        .catch(error => {
            return req.status(403).json({errors: error})
        })
}
