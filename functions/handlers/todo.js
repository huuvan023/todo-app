const { db,admin } = require("./../utils/admin");
const firebase = require('firebase');
//Import validate data function
const { isEmpty } = require('./../utils/validate');

exports.addTodo = (req, res) => {
    if (!isEmpty(req.body.title)) {
        const todo = {
            userHandle: req.user.email,
            title: req.body.title,
            status: false,
            createdAt: new  Date().toISOString()
        };
        db.collection("todos")
            .add(todo)
            .then((rs) => {
                return res.status(200).json({
                    message: `document ${rs.id} has been added successfully!`
                })
            })
            .catch(error => {
                res.status(500).json({
                    errors: error
                })
            })
    }
    else {
        return res.status(500).json({ errors: "Title must not be empty!" })
    }
}
exports.editTodo = (req, res) => {
    if (!isEmpty(req.body.title)) {
        const title = req.body.title;
        const param = req.params.todoID;

        const todoDoc = db.doc(`/todos/${param}`);

        todoDoc.get()
            .then(rs => {
                if (!rs.exists) {
                    return res.status(404).json({
                        errors: "Todo not found!"
                    });
                }
                else {
                    if( req.user.email !== rs.data().userHandle ) {
                        return res.status(403).json({ errors: "Unauthorized!" })
                    }
                    return todoDoc.update({ title: title})
                        .then(rs => {
                            return res.json({ message: "Update successfully!"})
                        })
                        .catch(error => {
                            return res.json({ errors:error.code })
                        })
                }
            })
            .catch( error => {
                res.json({ errors:error.code })
            })
    }
    else {
        return res.status(500).json({ errors: "Title must not be empty!" })
    }
}

exports.completeTodo = (req, res) => {
    const param = req.params.todoID;
    const status = req.body.status;
    const todoDoc = db.doc(`/todos/${param}`);

    todoDoc.get()
        .then(rs => {
            if (!rs.exists) {
                return res.status(404).json({
                    errors: "Todo not found!"
                });
            }
            else {
                if( req.user.email !== rs.data().userHandle ) {
                    return res.status(403).json({ errors: "Unauthorized!" })
                }
                return todoDoc.update({ status: status})
                    .then(rs => {
                        return res.json({ message: "Update status successfully!"})
                    })
                    .catch(error => {
                        return res.json({ errors:error.code })
                    })
            }
        })
        .catch( error => {
            res.json({ errors:error.code })
        })
}

exports.getAllTodos = (req, res) => {
    db.collection("todos")
        .where("userHandle","==", req.user.email)
        .orderBy("createdAt","desc").get()
        .then( data => {
            let todos = [];
            data.forEach( item => {
                todos.push({
                    todosID : item.id,
                    userHandle: item.data().userHandle,
                    createdAt: item.data().createdAt,
                    status: item.data().status,
                    title: item.data().title,
                });
            });
            return res.json(todos);
        })
        .catch( error => {
            res.status(500).json({ errors: `some thing was wrong! ${error}`})
        })
}
exports.deleteTodo = (req, res) => {
    const document = db.doc(`/todos/${req.params.todoID}`);

    document.get()
        .then(rs => {
            if (!rs.exists) {
                return res.status(404).json({
                    errors: "Todo not found!"
                });
            }
            else {
                if( req.user.email !== rs.data().userHandle ) {
                    return res.status(403).json({ errors: "Unauthorized!" })
                }
                return document.delete().then((rs) => {
                    return res.status(200).json({ message: `Delete successfully!` })
                })
            }
        })
        .catch(error => {
            res.status(500).json({ errors: error.code })
        })
}
