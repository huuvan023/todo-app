const { db,admin } = require("./../utils/admin");
const firebase = require('firebase');
//Import validate data function
const { isEmail, isEmpty } = require('./../utils/validate');
//user sign up
exports.SignUp = (req, res) => {
    const newUser = {
        email: req.body.email,
        password: req.body.password,
        confirmPassword: req.body.confirmPassword
    };
    let errors = {};
    if( isEmpty(newUser.email) ) {
        errors = "Email must not be empty!"
    }
    else if( !isEmail(newUser.email) ) {
        errors = "Email not valid!";
    }
    if( isEmpty(newUser.password) ) {
        errors = "Password must not be empty!";
    }
    if( newUser.password !== newUser.confirmPassword ) {
        errors = "Password not match!";
    }
    if (Object.keys(errors).length > 0) return res.status(400).json({ errors: errors });

    // eslint-disable-next-line promise/catch-or-return
    db.doc(`/users/${newUser.email}`).get()
        // eslint-disable-next-line promise/always-return
        .then( rs => {
            // eslint-disable-next-line no-empty,promise/always-return
            if( rs.exists ) {
                return res.status(400).json({ errors: 'This email already taken!' })
            }
            else {
                return firebase.auth().createUserWithEmailAndPassword(newUser.email,newUser.password);
            }
        })
        .then((rs) => {
            return rs.user.getIdToken();
        })
        .then(token => {
            const userCredeltial = {
                email: newUser.email,
                password: newUser.password,
                createdAt: new  Date().toISOString()
            };
            //Store user
            db.doc(`/users/${newUser.email}`)
                .set(userCredeltial);
            return token
        })
        .then(token => {
            res.status(201).json({
                token
            })
            return;
        })
        .catch(error => {
            //split code to 2 part
            let code = error.code.split("/");
            let search = '-';
            let replaceWith = ' ';
            //replace all "-" character to " "
            let result = code[1].split(search).join(replaceWith);
            res.status(500).json({
                //UpperCase the first character in string
                errors: result.charAt(0).toUpperCase() + result.slice(1) + "!",
            })
        });
};
//User login
exports.Login = (req, res) => {
    const user = {
        email: req.body.email,
        password: req.body.password,
    };
    let errors = {};
    if( isEmpty(user.email) ) {
        errors.email = "Email must not be empty!"
    }
    if( isEmpty(user.password) ) {
        errors.password = "Password must not be empty!"
    }
    if( Object.keys(errors).length > 0 ) return res.status(400).json({ errors });

    //Login
    firebase.auth().signInWithEmailAndPassword(user.email, user.password)
        .then(rs => {
            return rs.user.getIdToken();
        })
        .then( token => {
            return res.status(201).json({ token: `HV ${token}` })
        })
        .catch(error => {
            //split code to 2 part
            let code = error.code.split("/");
            let search = '-';
            let replaceWith = ' ';
            //replace all "-" character to " "
            let result = code[1].split(search).join(replaceWith);
            if( (result.charAt(0).toUpperCase() + result.slice(1)) === "User not found" || (result.charAt(0).toUpperCase() + result.slice(1)) === "Wrong password" ) {
                res.status(500).json({
                    //UpperCase the first character in string
                    errors: "Wrong email or password!",
                })
            }
            else {
                res.status(500).json({
                    //UpperCase the first character in string
                    errors: result.charAt(0).toUpperCase() + result.slice(1),
                })
            }
        });

}

