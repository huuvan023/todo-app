import jwtDecode from 'jwt-decode'
import axios from 'axios'

export function isAuthenticated (token) {
  if (token && token.startsWith('HV ')) {
    const decodedToken = jwtDecode(token.split('HV ')[1])
    if (decodedToken.exp * 1000 >= Date.now()) {
      axios.defaults.headers.common.Authorization = token
      return true
    } else {
      return false
    }
  } else {
    return false
  }
}
