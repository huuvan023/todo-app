import axios from 'axios'

export default function setAxiosHeaders (token) {
  axios.defaults.headers.common.Authorization = token
}
