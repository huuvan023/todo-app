export const user = (state) => {
  return state.user
}
export const getLoading = (state) => {
  return state.loading
}
export const getLoginStatus = (state) => {
  return state.statusLogin
}
export const getTodos = (state) => {
  return state.todos
}
export const getTodosComplete = (state) => {
  let count = 0
  state.todos.forEach(item => {
    if (item.status) {
      count++
    }
  })
  return count
}
export const getTodosDoing = (state) => {
  let count = 0
  state.todos.forEach(item => {
    if (!item.status) {
      count++
    }
  })
  return count
}
