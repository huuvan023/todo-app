import setAxiosHeaders from 'src/store/module-example/helpers'

export const LOGIN = (state, data) => {
  setAxiosHeaders(data.token)
  state.validate = true
  state.user = {
    email: data.email,
    password: data.password
  }
}
export const SET_LOADING = (state) => {
  state.loading = true
}
export const SET_DONE_LOADING = (state) => {
  state.loading = false
}
export const SET_STATUS_LOGIN = (state, status) => {
  state.statusLogin = status
}
export const SET_TODOS = (state, data) => {
  state.todos = data
}
