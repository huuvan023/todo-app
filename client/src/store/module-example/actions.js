import axios from 'axios'

export function login ({ commit }, data) {
  return axios.post('https://europe-west1-todo-app-a9587.cloudfunctions.net/api/login', {
    email: data.email,
    password: data.password
  })
    .then(result => {
      if (data.rememberMe) {
        localStorage.setItem('token', result.data.token)
        localStorage.setItem('email', data.email)
        localStorage.setItem('password', data.password)
      } else {
        sessionStorage.setItem('token', result.data.token)
        sessionStorage.setItem('email', data.email)
        sessionStorage.setItem('password', data.password)
      }
      return commit('LOGIN', {
        token: result.data.token,
        data: {
          email: data.email,
          password: data.password
        }
      })
    })
    .then(() => {
      return commit('SET_STATUS_LOGIN', {
        success: 'Login successfully!'
      })
    })
    .catch(error => {
      return commit('SET_STATUS_LOGIN', {
        fail: error.response.data.errors
      })
    })
}
export function setLoading ({ commit }) {
  return commit('SET_LOADING')
}
export function setDoneLoading ({ commit }) {
  return commit('SET_DONE_LOADING')
}
export function fetchTodos ({ commit }) {
  axios.get('https://europe-west1-todo-app-a9587.cloudfunctions.net/api/todos')
    .then(rs => {
      const data = []
      rs.data.forEach(item => {
        data.push(item)
      })
      return commit('SET_TODOS', data)
    })
    .then(() => {
      return commit('SET_DONE_LOADING')
    })
    .catch(error => {
      console.log(error.response)
    })
}
export function addTask ({ commit }, data) {
  return axios.post('https://europe-west1-todo-app-a9587.cloudfunctions.net/api/add', {
    title: data
  })
    .then(rs => {
      console.log(rs.data)
    })
    .then(() => {
      return axios.get('https://europe-west1-todo-app-a9587.cloudfunctions.net/api/todos')
        .then(rs => {
          const data = []
          rs.data.forEach(item => {
            data.push(item)
          })
          return commit('SET_TODOS', data)
        })
        .then(() => {
          return commit('SET_DONE_LOADING')
        })
        .catch(error => {
          console.log(error.response)
        })
    })
    .catch(error => {
      console.log(error.response.data)
    })
}
export function deleteTask ({ commit }, data) {
  return axios.post(`https://europe-west1-todo-app-a9587.cloudfunctions.net/api/delete/${data}`)
    .then(() => {
      return axios.get('https://europe-west1-todo-app-a9587.cloudfunctions.net/api/todos')
        .then(rs => {
          const data = []
          rs.data.forEach(item => {
            data.push(item)
          })
          return commit('SET_TODOS', data)
        })
        .then(() => {
          return commit('SET_DONE_LOADING')
        })
        .catch(error => {
          console.log(error.response)
        })
    })
    .catch(error => {
      console.log(error.response.da)
    })
}
export function completeTask ({ commit }, data) {
  return axios.post(`https://europe-west1-todo-app-a9587.cloudfunctions.net/api/changeStatus/${data.id}`, {
    status: !data.status
  })
    .then(() => {
      return axios.get('https://europe-west1-todo-app-a9587.cloudfunctions.net/api/todos')
        .then(rs => {
          const data = []
          if (rs.data.length > 0) {
            rs.data.forEach(item => {
              data.push(item)
            })
          }
          return commit('SET_TODOS', data)
        })
        .then(() => {
          return commit('SET_DONE_LOADING')
        })
        .catch(error => {
          console.log(error.response)
        })
    })
    .catch(error => {
      console.log(error.response.da)
    })
}
export function onClearComplete ({ commit, state }, data) {
  return state.todos.forEach(item => {
    if (item.status) {
      axios.post(`https://europe-west1-todo-app-a9587.cloudfunctions.net/api/delete/${item.todosID}`)
        .then(() => {
          return axios.get('https://europe-west1-todo-app-a9587.cloudfunctions.net/api/todos')
            .then(rs => {
              const data = []
              rs.data.forEach(item => {
                data.push(item)
              })
              return commit('SET_TODOS', data)
            })
            .then(() => {
              return commit('SET_DONE_LOADING')
            })
            .catch(error => {
              console.log(error.response)
            })
        })
        .catch(error => {
          console.log(error.response.da)
        })
    }
  })
}
