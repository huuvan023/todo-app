
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') }
    ],
    meta: { requireAuth: true }
  },
  {
    path: '/login',
    component: () => import('layouts/Login.vue'),
    meta: { requireAuth: false }
  },
  {
    path: '/signup',
    component: () => import('layouts/SignUp.vue'),
    meta: { requireAuth: false }
  },
  {
    path: '*',
    component: () => import('pages/Error404.vue'),
    meta: { requireAuth: false }
  }
]
export default routes
