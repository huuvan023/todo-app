import Vue from 'vue'
import VueRouter from 'vue-router'

import routes from './routes'
import { isAuthenticated } from '../Utils/auth'

Vue.use(VueRouter)

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default function () {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,

    // Leave these as they are and change in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })
  Router.beforeEach((to, from, next) => {
    if (to.matched.some(route => route.meta.requireAuth)) {
      if (localStorage.getItem('token')) {
        if (isAuthenticated(localStorage.getItem('token'))) {
          next()
        } else {
          next('/login')
        }
      } else {
        next('/login')
      }
    } else {
      if (localStorage.getItem('token')) {
        if (isAuthenticated(localStorage.getItem('token'))) {
          next('/')
        } else {
          next()
        }
      }
    }
  })
  return Router
}
